package com.neo.drools.utils;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;

/**
 * @author jinneng.liao
 */
public class DroolsUtils {

    public static KieContainer getKieContainer() {
        return KieServices.Factory.get().getKieClasspathContainer();
    }
}
