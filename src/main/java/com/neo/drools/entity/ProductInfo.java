package com.neo.drools.entity;

/**
 * @author jinneng.liao
 * 产品信息
 */
public class ProductInfo {
    private Long id;
    /**
     * 产品名称 = 产品中文商品名 + 产品通用名称 + 产品规格
     */
    private String productName;
    /**
     * 规格
     */
    private String spec;

    public ProductInfo(Long id, String productName, String spec) {
        this.id = id;
        this.productName = productName;
        this.spec = spec;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    @Override
    public String toString() {
        return "ProductInfo{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", spec='" + spec + '\'' +
                '}';
    }
}
