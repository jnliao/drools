package com.neo.drools.entity;

/**
 * @author jinneng.liao
 */
public class RuleDto {
    /**
     * 替换
     */
    public static final int REPLACE = 1;
    /**
     * 置空
     */
    public static final int EMPTY = 2;

    /**
     * 规则类型
     */
    private int ruleType;
    /**
     * 原始值
     */
    private String rawValue;
    /**
     * 标准值（置空的没有标准值）
     */
    private String standardValue;

    public RuleDto(int ruleType, String rawValue, String standardValue) {
        this.ruleType = ruleType;
        this.rawValue = rawValue;
        this.standardValue = standardValue;
    }

    public int getRuleType() {
        return ruleType;
    }

    public void setRuleType(int ruleType) {
        this.ruleType = ruleType;
    }

    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public String getStandardValue() {
        return standardValue;
    }

    public void setStandardValue(String standardValue) {
        this.standardValue = standardValue;
    }

    @Override
    public String toString() {
        return "RuleDto{" +
                "ruleType=" + ruleType +
                ", rawValue='" + rawValue + '\'' +
                ", standardValue='" + standardValue + '\'' +
                '}';
    }
}
